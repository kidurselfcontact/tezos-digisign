package com.sword.signature.daemon.job

import com.sword.signature.business.exception.EntityNotFoundException
import com.sword.signature.business.exception.ServiceException
import com.sword.signature.business.service.*
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import org.slf4j.LoggerFactory
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import java.time.OffsetDateTime

@Component
class DeferredSignatureJob(
    val deferredSignatureService: DeferredSignatureService,
    val signService: SignService,
    val algorithmService: AlgorithmService,
    val accountService: AccountService
) {

    @Scheduled(cron = "\${daemon.deferred.cron}")
    fun scheduledSignature() {
        runBlocking {
            sign()
        }
    }

    @Transactional(rollbackFor = [ServiceException::class])
    suspend fun sign() {
        LOGGER.debug("Starting deferred signature.")

        // get all files
        val files = deferredSignatureService.getFiles(dummyAdminAccount).toList()

        // for each user make a list of files
        val allBatch = files.groupBy { it.userId }

        if (allBatch.isEmpty()) {
            LOGGER.debug("There is no file to sign in a deferred way.")
            return
        }

        // call batchSign on each list
        val algorithm = algorithmService.getAlgorithmByName("SHA-256")
        allBatch.forEach { (key, value) ->
            val user = accountService.getAccount(dummyAdminAccount, key) ?: throw EntityNotFoundException("userID", key)

            val flowName = "batch-${OffsetDateTime.now()}"
            val fileHashes = value.asFlow().map { Pair(it.hash.lowercase(), it.metadata) }

            signService.batchSign(
                requester = user,
                algorithm = algorithm,
                flowName = flowName,
                fileHashes = fileHashes,
                channelName = null
            ).toList()

            value.forEach {
                deferredSignatureService.deleteSignatureFile(dummyAdminAccount, it.id)
            }
        }
    }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(DeferredSignatureService::class.java)
    }
}
