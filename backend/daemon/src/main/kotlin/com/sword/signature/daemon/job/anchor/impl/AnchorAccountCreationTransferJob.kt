package com.sword.signature.daemon.job.anchor.impl

import com.sword.signature.business.configuration.OptionalFeaturesConfig
import com.sword.signature.business.exception.EntityNotFoundException
import com.sword.signature.business.exception.FeatureNotEnabledException
import com.sword.signature.business.model.Account
import com.sword.signature.business.model.JobPatch
import com.sword.signature.business.model.TezosAccount
import com.sword.signature.business.model.integration.JobPayloadType
import com.sword.signature.business.model.integration.TezosOperationAnchorJobPayload
import com.sword.signature.business.model.integration.TezosOperationValidationJobPayload
import com.sword.signature.business.service.AccountService
import com.sword.signature.business.service.JobService
import com.sword.signature.common.enums.JobStateType
import com.sword.signature.common.enums.TezosAccountStateType
import com.sword.signature.daemon.configuration.TezosConfig
import com.sword.signature.daemon.job.Metrics
import com.sword.signature.daemon.job.anchor.TezosOperationAnchorJob
import com.sword.signature.daemon.logger
import com.sword.signature.daemon.sendPayload
import com.sword.signature.model.entity.RevealJobEntity
import com.sword.signature.model.repository.RevealJobRepository
import eu.coexya.tezos.connector.model.TezosIdentity
import eu.coexya.tezos.connector.service.TezosReaderService
import eu.coexya.tezos.connector.service.TezosUtilService
import eu.coexya.tezos.connector.service.TezosWriterService
import kotlinx.coroutines.reactive.awaitSingle
import org.springframework.beans.factory.annotation.Value
import org.springframework.messaging.MessageChannel
import org.springframework.messaging.support.MessageBuilder
import org.springframework.stereotype.Component
import java.time.OffsetDateTime

@Component
class AnchorAccountCreationTransferJob(
    override val accountService: AccountService,
    override val tezosWriterService: TezosWriterService,
    override val tezosReaderService: TezosReaderService,
    override val tezosUtilService: TezosUtilService,
    override val jobService: JobService,
    override val anchoringRetryMessageChannel: MessageChannel,
    override val validationMessageChannel: MessageChannel,
    override val tezosConfig: TezosConfig,
    override val metrics: Metrics,

    private val optionalFeaturesConfig: OptionalFeaturesConfig,
    private val revealJobRepository: RevealJobRepository,
    private val anchoringMessageChannel: MessageChannel,
    @Value("\${daemon.anchoring.maxTry}") private val maxTry: Int
) : TezosOperationAnchorJob {

    override suspend fun startAnchor(payload: TezosOperationAnchorJobPayload) {
        if (!optionalFeaturesConfig.keyGeneration.enabled) {
            throw FeatureNotEnabledException()
        }

        val requesterId = payload.requesterId
        val jobId = payload.jobId
        LOGGER.info("Starting anchoring process of TransferJob (id = $jobId), for requester (id = $requesterId)")

        val requester = getAccount(requesterId)
        val transferJob =
            jobService.findByIdTransferJob(requester, jobId) ?: throw EntityNotFoundException("job", jobId)

        // Check job status
        if (transferJob.state != JobStateType.INSERTED) {
            LOGGER.info("TransferJob '{}' is '{}' so it cannot be anchored", transferJob.id, transferJob.state)
            return
        }

        val accountToUpdate = getAccount(transferJob.accountToUpdate)

        val defaultAmount = optionalFeaturesConfig.keyGeneration.defaultAmount

        if (defaultAmount < optionalFeaturesConfig.keyGeneration.revealCost) {
            rejectJob("Default amount cannot permit a reveal for a new account", requester, jobId)
            return
        }

        val defaultProviderPublicKey = optionalFeaturesConfig.keyGeneration.defaultProviderPublicKey
        val defaultProviderSecretKey = optionalFeaturesConfig.keyGeneration.defaultProviderSecretKey

        if (defaultProviderPublicKey.isNullOrBlank() || defaultProviderSecretKey.isNullOrBlank()) {
            rejectJob("Default provider has not been set", requester, jobId)
            return
        }

        try {
            val defaultProviderIdentity = tezosUtilService.retrieveIdentity(
                publicKeyBase58 = defaultProviderPublicKey,
                secretKeyBase58 = defaultProviderSecretKey
            )
            val defaultProviderPublicAddress = defaultProviderIdentity.publicAddress
            if (tezosReaderService.getTezosAccountBalance(defaultProviderPublicAddress) < defaultAmount) {
                rejectJob(
                    "Default provider (public address = $defaultProviderPublicAddress)" +
                            " do not have enough tez to create an account with default amount = $defaultAmount micro tez",
                    requester,
                    jobId
                )
                return
            }

            // Generate or get new tezos identity (locally)
            val newTezosIdentity = generateNewIdentity(accountToUpdate)

            // New identity has not been set properly
            if (newTezosIdentity == null) {
                rejectJob(
                    "Tezos keys have not been properly defined in account with id = ${accountToUpdate.id}",
                    requester,
                    jobId
                )
                return
            }

            if (tezosReaderService.getTezosAccountBalance(newTezosIdentity.publicAddress) >= defaultAmount) {
                LOGGER.info(
                    "Generated tezos account has already been supplied, skipping transfer to directly reveal the account."
                )
                revealAccount(requester, transferJob.accountToUpdate)
                return
            }

            LOGGER.info(
                "Ready to anchor TransferJob '$jobId' from default provider '$defaultProviderPublicAddress' " +
                        "to generated account '${newTezosIdentity.publicAddress}' " +
                        "with amount '$defaultAmount' micro tez into the blockchain."
            )

            // Transfer a certain amount of tez to the newly created account
            val transactionHash = tezosWriterService.transferTezos(
                from = defaultProviderIdentity,
                to = newTezosIdentity.publicAddress,
                amount = defaultAmount
            )

            val injectedJob = jobService.patchTransferJob(
                requester = requester,
                jobId = jobId,
                patch = JobPatch(
                    transactionHash = transactionHash,
                    state = JobStateType.INJECTED,
                    numberOfTry = transferJob.numberOfTry + 1
                )
            )

            validationMessageChannel.sendPayload(
                TezosOperationValidationJobPayload(
                    type = JobPayloadType.ACCOUNT_CREATION_TRANSFER,
                    requesterId = requesterId,
                    jobId = jobId,
                    transactionHash = transactionHash,
                    lastlyCheckedLevel = getHeadBlockLevel(),
                    injectionTime = injectedJob.injectedDate!!
                )
            )
        } catch (e: Exception) {
            LOGGER.error("Anchoring try number {} of job ({}) failed.", transferJob.numberOfTry, jobId, e)

            if (transferJob.numberOfTry < maxTry) {
                LOGGER.error("Anchoring of job ({}) will be retried later.", jobId)
                // Increment the try counter.
                jobService.patchTransferJob(
                    requester = requester,
                    jobId = jobId,
                    patch = JobPatch(
                        numberOfTry = transferJob.numberOfTry + 1
                    )
                )
                // Set the anchoring for retry later.
                anchoringRetryMessageChannel.sendPayload(payload)
                // Rethrow the exception.
                throw e
            } else {
                LOGGER.error("Last anchoring try of job ({}) failed", jobId)
            }
        }

    }

    /**
     * If we retry anchoring the first transfer to create the account,
     * the new identity has already been generated so we return it.
     */
    private suspend fun generateNewIdentity(accountToUpdate: Account): TezosIdentity? {
        if (accountToUpdate.publicKey.isNullOrEmpty() && accountToUpdate.privateKey.isNullOrEmpty()) {
            val newTezosIdentity = tezosWriterService.generateNewIdentity()

            val tezosAccount = TezosAccount(
                publicKey = newTezosIdentity.publicKey,
                privateKey = newTezosIdentity.privateKey,
            )

            accountService.setTezosAccount(accountToUpdate.id, tezosAccount, TezosAccountStateType.GENERATED)
            return newTezosIdentity
        } else {
            return if (accountToUpdate.publicKey.isNullOrEmpty() || accountToUpdate.privateKey.isNullOrEmpty()
                || accountToUpdate.tezosAccountState != TezosAccountStateType.GENERATED
            ) {
                null
            } else {
                tezosUtilService.retrieveIdentity(
                    publicKeyBase58 = accountToUpdate.publicKey!!,
                    secretKeyBase58 = accountToUpdate.privateKey!!
                )
            }
        }
    }

    private suspend fun revealAccount(requester: Account, accountToUpdateId: String) {
        // The second thing to do when creating a tezos account is reveal their public key to the chain.
        // Creating the RevealJob.
        val revealJobEntity = revealJobRepository.insert(
            RevealJobEntity(
                state = JobStateType.INSERTED,
                stateDate = OffsetDateTime.now(),
                userId = requester.id,
                accountToUpdate = accountToUpdateId
            )
        ).awaitSingle()

        // Send a message to the anchoring channel to trigger the daemon anchoring job
        anchoringMessageChannel.send(
            MessageBuilder.withPayload(
                TezosOperationAnchorJobPayload(
                    type = JobPayloadType.REVEAL,
                    requesterId = requester.id,
                    jobId = revealJobEntity.id!!
                )
            ).build()
        )
    }

    companion object {
        val LOGGER = logger()
    }

}
