package com.sword.signature.daemon.job.anchor.impl

import com.sword.signature.business.configuration.OptionalFeaturesConfig
import com.sword.signature.business.exception.EntityNotFoundException
import com.sword.signature.business.exception.FeatureNotEnabledException
import com.sword.signature.business.model.Account
import com.sword.signature.business.model.JobPatch
import com.sword.signature.business.model.RevealJob
import com.sword.signature.business.model.integration.JobPayloadType
import com.sword.signature.business.model.integration.TezosOperationAnchorJobPayload
import com.sword.signature.business.model.integration.TezosOperationValidationJobPayload
import com.sword.signature.business.service.AccountService
import com.sword.signature.business.service.JobService
import com.sword.signature.common.enums.JobStateType
import com.sword.signature.daemon.configuration.TezosConfig
import com.sword.signature.daemon.job.Metrics
import com.sword.signature.daemon.job.anchor.TezosOperationAnchorJob
import com.sword.signature.daemon.logger
import com.sword.signature.daemon.sendPayload
import eu.coexya.tezos.connector.service.TezosReaderService
import eu.coexya.tezos.connector.service.TezosUtilService
import eu.coexya.tezos.connector.service.TezosWriterService
import org.springframework.beans.factory.annotation.Value
import org.springframework.messaging.MessageChannel
import org.springframework.stereotype.Component

@Component
class AnchorRevealJob(
    override val accountService: AccountService,
    override val tezosWriterService: TezosWriterService,
    override val tezosReaderService: TezosReaderService,
    override val tezosUtilService: TezosUtilService,
    override val jobService: JobService,
    override val anchoringRetryMessageChannel: MessageChannel,
    override val validationMessageChannel: MessageChannel,
    override val tezosConfig: TezosConfig,
    override val metrics: Metrics,

    private val optionalFeaturesConfig: OptionalFeaturesConfig,
    @Value("\${daemon.anchoring.maxTry}") private val maxTry: Int
) : TezosOperationAnchorJob {

    override suspend fun startAnchor(payload: TezosOperationAnchorJobPayload) {
        if (!optionalFeaturesConfig.keyGeneration.enabled) {
            throw FeatureNotEnabledException()
        }

        val requesterId = payload.requesterId
        val jobId = payload.jobId
        LOGGER.info("Starting anchoring process of RevealJob (id = $jobId), for requester (id = $requesterId)")

        val requester = getAccount(requesterId)
        val job: RevealJob =
            jobService.findByIdRevealJob(requester, jobId) ?: throw EntityNotFoundException("job", jobId)

        val accountToUpdate = getAccount(job.accountToUpdate)

        // Check job status
        if (job.state != JobStateType.INSERTED) {
            LOGGER.info("RevealJob '{}' is '{}' so it cannot be anchored", job.id, job.state)
            return
        }

        if (accountToUpdate.publicKey.isNullOrEmpty() || accountToUpdate.privateKey.isNullOrEmpty() || accountToUpdate.hash.isNullOrEmpty()) {
            rejectJob(
                "Account with id = ${accountToUpdate.id} should have an associated tezos account",
                requester,
                jobId
            )
            return
        }

        try {

            val unRevealedIdentity = tezosUtilService.retrieveIdentity(
                publicKeyBase58 = accountToUpdate.publicKey!!,
                secretKeyBase58 = accountToUpdate.privateKey!!
            )

            // Sanity check, tez balance of credited account should allow to reveal.
            if (tezosReaderService.getTezosAccountBalance(unRevealedIdentity.publicAddress)
                <= optionalFeaturesConfig.keyGeneration.revealCost
            ) {
                rejectJob(
                    "New account (publicAddress = ${unRevealedIdentity.publicAddress} should have enough tez to reveal",
                    requester,
                    jobId
                )
                return
            }

            LOGGER.debug(
                "Ready to anchor RevealJob '{}' publicAddress '{}' into the blockchain.",
                jobId, unRevealedIdentity.publicAddress
            )

            val transactionHash = tezosWriterService.revealIdentity(unRevealedIdentity)

            val injectedJob = jobService.patchRevealJob(
                requester = requester,
                jobId = jobId,
                patch = JobPatch(
                    transactionHash = transactionHash,
                    state = JobStateType.INJECTED,
                    numberOfTry = job.numberOfTry + 1
                )
            )

            validationMessageChannel.sendPayload(
                TezosOperationValidationJobPayload(
                    type = JobPayloadType.REVEAL,
                    requesterId = requesterId,
                    jobId = jobId,
                    transactionHash = transactionHash,
                    lastlyCheckedLevel = getHeadBlockLevel(),
                    injectionTime = injectedJob.injectedDate!!
                )
            )
        } catch (e: Exception) {
            LOGGER.error("Anchoring try number {} of job ({}) failed.", job.numberOfTry, jobId, e)

            if (job.numberOfTry < maxTry) {
                LOGGER.error("Anchoring of job ({}) will be retried later.", jobId)
                // Increment the try counter.
                jobService.patchRevealJob(
                    requester = requester,
                    jobId = jobId,
                    patch = JobPatch(
                        numberOfTry = job.numberOfTry + 1
                    )
                )
                // Set the anchoring for retry later.
                anchoringRetryMessageChannel.sendPayload(payload)
                // Rethrow the exception.
                throw e
            } else {
                LOGGER.error("Last anchoring try of job ({}) failed", jobId)
            }
        }


    }

    companion object {
        val LOGGER = logger()
    }

}
