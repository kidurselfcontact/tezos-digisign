package com.sword.signature.daemon.job.anchor.impl

import com.sword.signature.business.exception.EntityNotFoundException
import com.sword.signature.business.model.Account
import com.sword.signature.business.model.Job
import com.sword.signature.business.model.JobPatch
import com.sword.signature.business.model.integration.JobPayloadType
import com.sword.signature.business.model.integration.TezosOperationAnchorJobPayload
import com.sword.signature.business.model.integration.TezosOperationValidationJobPayload
import com.sword.signature.business.service.AccountService
import com.sword.signature.business.service.JobService
import com.sword.signature.common.enums.JobStateType
import com.sword.signature.daemon.configuration.KeyLockStore
import com.sword.signature.daemon.configuration.TezosConfig
import com.sword.signature.daemon.job.Metrics
import com.sword.signature.daemon.job.anchor.TezosOperationAnchorJob
import com.sword.signature.daemon.logger
import com.sword.signature.daemon.sendPayload
import eu.coexya.tezos.connector.service.TezosReaderService
import eu.coexya.tezos.connector.service.TezosUtilService
import eu.coexya.tezos.connector.service.TezosWriterService
import org.springframework.beans.factory.annotation.Value
import org.springframework.messaging.MessageChannel
import org.springframework.stereotype.Component

@Component
class AnchorSignatureJob(
    override val accountService: AccountService,
    override val tezosWriterService: TezosWriterService,
    override val tezosReaderService: TezosReaderService,
    override val tezosUtilService: TezosUtilService,
    override val jobService: JobService,
    override val anchoringRetryMessageChannel: MessageChannel,
    override val validationMessageChannel: MessageChannel,
    override val tezosConfig: TezosConfig,
    override val metrics: Metrics,

    @Value("\${tezos.contract.address}") private val contractAddress: String,
    @Value("\${daemon.anchoring.maxTry}") private val maxTry: Int,
    private val keyLockStore: KeyLockStore,
) : TezosOperationAnchorJob {

    override suspend fun startAnchor(payload: TezosOperationAnchorJobPayload) {
        val requesterId = payload.requesterId
        val jobId = payload.jobId

        val requester = getAccount(requesterId)
        val job: Job = getJob(requester, jobId)
        val rootHash: String? = job.rootHash

        val lock = keyLockStore.getLock(requesterId)
        if (!lock.tryLock()) {
            LOGGER.debug("Lock is already used for requester '{}'", requesterId)
            // Set the anchoring for retry later.
            anchoringRetryMessageChannel.sendPayload(payload)
            return
        }

        try {
            // Check job status
            if (job.state != JobStateType.INSERTED) {
                LOGGER.info("Job '{}' is '{}' so it cannot be anchored", job.id, job.state)
                return
            }

            LOGGER.debug("Check existing root hash {} on the blockchain.", rootHash)
            if (rootHash == null || tezosReaderService.hashAlreadyExists(contractAddress, rootHash)) {

                if (rootHash == null) {
                    rejectJob(
                        "Job '$jobId' got rejected due to its rootHash being null.",
                        requester,
                        jobId
                    )
                } else {
                    rejectJob(
                        "Job '$jobId' got rejected due to its rootHash '$rootHash' being already on the blockchain",
                        requester,
                        jobId
                    )
                }
                return
            }

            LOGGER.debug("Ready to anchor job '{}' rootHash '{}' into the blockchain.", jobId, rootHash)

            val signerIdentity = checkTezosIdentity(requester)
            val transactionHash = tezosWriterService.anchorHash(rootHash = rootHash, signerIdentity = signerIdentity)

            val injectedJob = jobService.patchSignatureJob(
                requester = requester,
                jobId = jobId,
                patch = JobPatch(
                    transactionHash = transactionHash,
                    state = JobStateType.INJECTED,
                    numberOfTry = job.numberOfTry + 1,
                    contractAddress = contractAddress,
                    signerAddress = signerIdentity.publicAddress,
                )
            )

            validationMessageChannel.sendPayload(
                TezosOperationValidationJobPayload(
                    type = JobPayloadType.SIGNATURE,
                    requesterId = requesterId,
                    jobId = jobId,
                    transactionHash = transactionHash,
                    lastlyCheckedLevel = getHeadBlockLevel(),
                    injectionTime = injectedJob.injectedDate!!
                )
            )
        } catch (e: Exception) {
            LOGGER.error("Anchoring try number {} of job ({}) failed.", job.numberOfTry, jobId, e)

            if (job.numberOfTry < maxTry) {
                LOGGER.error("Anchoring of job ({}) will be retried later.", jobId)
                // Increment the try counter.
                jobService.patchSignatureJob(
                    requester = requester,
                    jobId = jobId,
                    patch = JobPatch(
                        numberOfTry = job.numberOfTry + 1
                    )
                )
                // Set the anchoring for retry later.
                anchoringRetryMessageChannel.sendPayload(payload)
                // Rethrow the exception.
                throw e
            } else {
                LOGGER.error("Last anchoring try of job ({}) failed", jobId)
            }
        } finally {
            // Free the lock for the requester.
            lock.unlock()
        }

    }

    private suspend fun getJob(requester: Account, jobId: String): Job {
        return jobService.findByIdSignatureJob(requester, jobId, true) ?: throw EntityNotFoundException("job", jobId)
    }

    companion object {
        val LOGGER = logger()
    }

}
