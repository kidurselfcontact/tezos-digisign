import smartpy as sp


class HashTimestampingSC1(sp.Contract):

    def __init__(self):
        self.init(
            storage=sp.big_map(
                l={},
                tkey=sp.TString,
                tvalue=sp.TPair(sp.TTimestamp, sp.TAddress)
            )
        )

    @sp.entry_point(name="default")
    def add_hash(self, hash):
        sp.verify(~self.data.storage.contains(hash), "Record hash already written")
        current = sp.now
        self.data.storage[hash] = (current, sp.sender)

@sp.add_test(name="add_hash")
def add_hash_test():
    alice = sp.test_account("Alice")
    hash = "7bb2682481344d4130aecd1f89ddb71b7da760b5d695ede3e0ee599efee8a6ba"

    sc = sp.test_scenario()
    contract = HashTimestampingSC1()
    sc += contract

    sc.h1("Add a new hash in the storage")
    contract.add_hash(hash).run(
        sender = alice.address
    )
    sc.p("Storage").show(contract.data.storage)
    sc.verify(contract.data.storage.contains(hash))


    sc.h1("Add an already existing hash")
    contract.add_hash(hash).run(
        sender = alice.address,
        valid = False,
        exception = "Record hash already written"
    )
    sc.p("Storage").show(contract.data.storage)


sp.add_compilation_target("hash_timestamping_SC1", HashTimestampingSC1())
