[Index](../README.md) 

# Tezos Digisign - Very Quick Start

This very quick start deployment does not need any compilation. It uses the docker images in docker hub.
To build the VM with the requirement, it's possible to use vagrant+VirtualBox. A vagrant file with the right external port and ansible script is available: [deployment](../deployment/vagrant).  
This deployment is based on the Granadanet Tezos Network. The Tezos account signing the transaction is already configured (please, use it sparingly).  

## Requirements

* Docker (for development) and Docker compose.

## VM deployment with vagrant

VirtualBox and Vagrant must have been installed on your computer.
After downloading [deployment](../deployment/vagrant), go into the directory vagrant and launch the command:  
`vagrant up`
  
connect to the VM:  
`vagrant ssh`  



## Deployment

Retrieve the code digisign  
`git clone https://gitlab.com/coexya/tezos/tezos-digisign.git`  
`cd tezos-digisign`  

Launch Tezos Digisign:    
`docker-compose -f docker-compose.testnet.d.yml up -d`
  
In a Web browser, you can connect to Digisign:     
  
Connect to the website (the login/password is admin/Sword@35):  
`http://localhost:8080/index.html#/signature-check`
  
Connect to the Tezos Index and check the storage (after signing a document):  
`https://ghost.tzstats.com/KT1PTTUwkPMDrwAtNfD3JZP93wH1DkycXPSH`

Connect to the Tezos Index and check the signer account:  
`https://ghost.tzstats.com/tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb`

  
Connect to the database with mongo express:  
`http://localhost:8081/#/login`
  
Check the api with swagger:  
`http://localhost:8080/api/swagger-ui.html`
  
Check the fake SMTP server:  
`http://localhost:5080/`


## Stop the VM with vagrant

Quit the linux machine:  
`exit`  

Stop the VM:  
`vagrant halt`  


## Mainnet
There is also the same way of deployment for Mainnet.  
Beforehand, you have to update the configuration file to put the public/secret keys:  **compose-config/mainnet/application-daemon.yml**.


Launch Tezos Digisign:    
`docker-compose -f docker-compose.mainnet.d.yml up -d`

For mainnet, you have to modify the docker-compose.mainnet.d.yml file in order to remove mongo-express for security reason.
