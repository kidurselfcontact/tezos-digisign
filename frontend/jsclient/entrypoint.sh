#!/usr/bin/env sh

# Update jsclient/build/dist/js/app file
sed -i "s|, *Bh *= *\"[^\"]*\" *,|,Bh=\"${REST_URL}\",|" /usr/share/nginx/html/js/app.*.js
sed -i "s| *secure *: *! *.|secure:\!${UNSECURED_TOKEN}|" /usr/share/nginx/html/js/app.*.js

# Run nginx
exec nginx -g 'daemon off;'
