import {NavigationGuardNext, Route} from "vue-router"
import modules from "@/store/modules"

export function loadOptionalFeatures(to: Route, from: Route, next: NavigationGuardNext) {
    if (modules.optionalFeatures.optionalFeatures !== undefined) {
        next()
    } else {
        modules.optionalFeatures.loadOptionalFeatures().then(() => {
            next()
        }).catch(() => next())
    }
}
