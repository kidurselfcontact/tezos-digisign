package com.sword.signature.api.custom


data class OptionalFeatures(
    val registering: Registering,
    val keyGeneration: KeyGeneration
) {

    // Features
    data class Registering(
        val enabled: Boolean
    )

    data class KeyGeneration(
        val enabled: Boolean
    )

}


