package com.sword.signature.api.sign

data class RetrySignRequest(
    val jobId: String,
)
