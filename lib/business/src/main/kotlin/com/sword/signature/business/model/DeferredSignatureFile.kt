package com.sword.signature.business.model

data class DeferredSignatureFile(
    val id: String,
    val userId: String,
    val hash: String,
    val metadata: FileMetadata
)
