package com.sword.signature.business.model.integration

import java.io.Serializable
import java.time.OffsetDateTime

data class CallBackJobMessagePayload(
    val jobId: String,
    val url: String,
    val numberOfTry: Int = 1
) : Serializable

data class TezosOperationAnchorJobPayload(
    val type: JobPayloadType,
    val requesterId: String,
    val jobId: String
) : Serializable

data class TezosOperationValidationJobPayload(
    val type: JobPayloadType,
    val requesterId: String,
    val jobId: String,
    val transactionHash: String,
    val lastlyCheckedLevel: Long,
    val injectionTime: OffsetDateTime
) : Serializable

enum class JobPayloadType(val value: String) {
    SIGNATURE("SIGNATURE"), ACCOUNT_CREATION_TRANSFER("ACCOUNT_CREATION_TRANSFER"), REVEAL("REVEAL")
}
