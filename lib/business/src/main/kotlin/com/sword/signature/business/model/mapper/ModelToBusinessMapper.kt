package com.sword.signature.business.model.mapper

import com.sword.signature.business.model.*
import com.sword.signature.common.enums.TreeElementType
import com.sword.signature.model.entity.*

fun AccountEntity.toBusiness() = Account(
    id = id!!,
    login = login,
    email = email,
    password = password,
    fullName = fullName,
    company = company,
    country = country,
    publicKey = publicKey,
    privateKey = privateKey,
    hash = hash,
    isAdmin = isAdmin,
    disabled = disabled,
    firstLogin = firstLogin,
    signatureLimit = signatureLimit,
    tezosAccountState = tezosAccountState
)

fun Metadata.toBusiness() = FileMetadata(
    fileName = fileName,
    fileSize = fileSize,
    customFields = customFields
)

fun DeferredSignatureFileEntity.toBusiness() = DeferredSignatureFile(
    id = id!!,
    userId = userId,
    hash = hash,
    metadata = metadata.toBusiness()
)

fun TreeElementEntity.toBusiness(job: Job): TreeElement = when (this.type) {
    TreeElementType.NODE -> TreeElement.NodeTreeElement(
        id = id!!,
        job = job,
        hash = hash,
        parentId = parentId,
        position = position
    )
    TreeElementType.LEAF -> TreeElement.LeafTreeElement(
        id = id!!,
        job = job,
        metadata = metadata!!.toBusiness(),
        hash = hash,
        parentId = parentId,
        position = position
    )
}

fun TezosOperationJobEntity.toBusiness() = when (this) {
    is TransferJobEntity -> this.toBusiness()
    is RevealJobEntity -> this.toBusiness()
    is JobEntity -> this.toBusiness()
}

fun TransferJobEntity.toBusiness() = TransferJob(
    id = id!!,
    transactionHash = transactionHash,
    timestamp = timestamp,
    blockDepth = blockDepth,
    blockHash = blockHash,
    createdDate = createdDate,
    injectedDate = injectedDate,
    numberOfTry = numberOfTry,
    validatedDate = validatedDate,
    state = state,
    stateDate = stateDate,
    signerAddress = signerAddress,
    userId = userId,
    amount = amount,
    accountToUpdate = accountToUpdate
)

fun RevealJobEntity.toBusiness() = RevealJob(
    id = id!!,
    transactionHash = transactionHash,
    timestamp = timestamp,
    blockDepth = blockDepth,
    blockHash = blockHash,
    createdDate = createdDate,
    injectedDate = injectedDate,
    numberOfTry = numberOfTry,
    validatedDate = validatedDate,
    state = state,
    stateDate = stateDate,
    signerAddress = signerAddress,
    userId = userId,
    accountToUpdate = accountToUpdate
)

fun JobEntity.toBusiness(rootHash: String? = null, files: List<TreeElement.LeafTreeElement>? = null) = Job(
    id = id!!,
    algorithm = algorithm,
    emailNotification = emailNotification,
    origin = origin,
    userId = userId,
    transactionHash = transactionHash,
    timestamp = timestamp,
    blockDepth = blockDepth,
    blockHash = blockHash,
    createdDate = createdDate,
    injectedDate = injectedDate,
    numberOfTry = numberOfTry,
    validatedDate = validatedDate,
    flowName = flowName,
    state = state,
    stateDate = stateDate,
    rootHash = rootHash,
    files = files,
    callBackUrl = callBackUrl,
    callBackStatus = callBackStatus,
    contractAddress = contractAddress,
    channelName = channelName,
    docsNumber = docsNumber,
    signerAddress = signerAddress,
    customFields = customFields
)

fun AlgorithmEntity.toBusiness() = Algorithm(
    id = id!!,
    name = name,
    digestLength = digestLength
)

fun TokenEntity.toBusiness() = Token(
    id = id!!,
    name = name,
    jwtToken = jwtToken,
    expirationDate = expirationDate,
    creationDate = creationDate,
    accountId = accountId,
    revoked = revoked
)
