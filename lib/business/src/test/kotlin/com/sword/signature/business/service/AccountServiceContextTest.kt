package com.sword.signature.business.service

import com.ninjasquad.springmockk.MockkBean
import com.sword.signature.business.exception.*
import com.sword.signature.business.model.Account
import com.sword.signature.business.model.AccountCreate
import com.sword.signature.business.model.AccountPatch
import com.sword.signature.business.model.TezosAccount
import com.sword.signature.business.service.impl.AccountServiceImpl
import com.sword.signature.model.migration.MigrationHandler
import eu.coexya.tezos.connector.node.NodeConnector
import io.mockk.clearMocks
import io.mockk.coEvery
import kotlinx.coroutines.flow.count
import kotlinx.coroutines.reactive.awaitSingle
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.DuplicateKeyException
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.nio.file.Path
import java.util.stream.Stream


@ExtendWith(SpringExtension::class)
class AccountServiceContextTest @Autowired constructor(
    private val accountService: AccountService,
    override val mongoTemplate: ReactiveMongoTemplate,
    override val migrationHandler: MigrationHandler
) : AbstractServiceContextTest() {

    @MockkBean
    private lateinit var nodeConnector: NodeConnector

    private val nonAdminAccount = Account(
        id = "nonAdmin",
        login = "nonAdminLogin",
        password = "nonAdminPassword",
        fullName = "nonAdmin",
        email = "nonAdmin@signature.com",
        company = null,
        country = null,
        publicKey = null,
        privateKey = null,
        hash = null,
        isAdmin = false,
        disabled = false,
        firstLogin = false,
        signatureLimit = null
    )

    private val adminAccount = dummyAdminAccount.copy(
        id = "admin",
        login = "adminLogin",
        password = "adminPassword",
        fullName = "admin",
        email = "admin@signature.com",
    )

    private var accountsInitialCount: Long = 0L

    private val accountId1 = "5e734ba4b075db359ea73a68"
    private val accountLogin1 = "account1"
    private val accountEmail1 = "account1@signature.com"
    private val accountPassword1 = "password"
    private val accountFullName1 = "Account 1"
    private val accountPublicKey1 = "publicKey"
    private val accountPrivateKey1 = "privateKey"
    private val accountHash1 = "hash"

    private val account1 = nonAdminAccount.copy(
        id = accountId1,
        login = accountLogin1,
        password = accountPassword1,
        fullName = accountFullName1,
        email = accountEmail1,
        publicKey = accountPublicKey1,
        privateKey = accountPrivateKey1,
        hash = accountHash1
    )

    private val accountId3 = "5e7381bb99dc7fd31dcc48c0"
    private val accountId4 = "5e7381a2b149856b56388bc4"
    private val accountId5 = "5e7381b2b187856b56388bc5"
    private val accountId6 = "5e7381c2b177856b26378bc6"
    private val accountId7 = "5e7381c2b177856b26378bc7"

    private val accountNonexistentId = "notExistentId"

    // TEZOS Accounts
    private val bobPublicAddress = "tz1aSkwEot3L2kmUvcoxzjMomb9mvBNuzFK6"
    private val bobPublicKey = "edpkurPsQ8eUApnLUJ9ZPDvu98E8VNj4KtJa1aZr16Cr5ow5VHKnz4"
    private val bobPrivateKey = "edsk3RFfvaFaxbHx8BMtEW1rKQcPtDML3LXjNqMNLCzC3wLC1bWbAt"

    private val alicePublicAddress = "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb"
    private val alicePublicKey = "edpkvGfYw3LyB1UcCahKQk4rF2tvbMUk8GFiTuMjL75uGXrpvKXhjn"
    private val alicePrivateKey = "edsk3QoqBuvdamxouPhin7swCvkQNgq4jP5KZPbwWNnwdZpSpJiEbq"

    private val nonExistentPublicAddress = "tz1NM8ybLHMV45WHxJRnCSPexooubehSVXW6"
    private val nonExistentPublicKey = "edpktqaKCqBmCuWxVUewf1iFEahpiDUA7dfWwYunrTNTFsUZfWmiKi"
    private val nonExistentPrivateKey = "edsk47UY67kTBH5fEiUgzx3bqGp46i5MoKg9e18XQDieH6RuV5yBqQ"

    private val incorrectPublicKey = "incorrectpublickeyqzdqz5z24za2a8"
    private val incorrectPrivateKey = "incorrectprivatekeyqzdqz5z24za2a8"

    fun resetMockks() {
        clearMocks(nodeConnector)
        coEvery { nodeConnector.getBalance(bobPublicAddress) } returns 100
        coEvery { nodeConnector.getBalance(nonExistentPublicAddress) } returns 0
    }

    @BeforeEach
    fun refreshDatabase() {
        resetDatabase()
        importJsonDatasets(Path.of("src/test/resources/datasets/accounts.json"))
        if (accountsInitialCount == 0L) {
            accountsInitialCount =
                runBlocking { mongoTemplate.getCollection("accounts").awaitSingle().countDocuments().awaitSingle() }
        }
        resetMockks()
    }

    @Nested
    inner class CreateAccountTest {

        private val newLogin = "test"
        private val newEmail = "test@test.com"
        private val newPassword = "secured"
        private val newFullName = "fullName"
        private val newCompany = "Great Company"
        private val newCountry = "France"

        // Tezos account bob
        private val newTezosAccount = TezosAccount(
            publicKey = bobPublicKey,
            privateKey = bobPrivateKey
        )

        @Test
        fun createAccountTest() {
            val toCreate = AccountCreate(
                login = newLogin, email = newEmail, password = newPassword, fullName = newFullName,
                company = newCompany, country = newCountry, tezosAccount = newTezosAccount
            )

            runBlocking {
                val createdAccount = accountService.createAccount(adminAccount, toCreate)

                assertAll("createdAccount",
                    { assertEquals(newLogin, createdAccount.login) },
                    { assertEquals(newEmail, createdAccount.email) },
                    { assertEquals(newPassword, createdAccount.password) },
                    { assertEquals(newFullName, createdAccount.fullName) },
                    { assertEquals(newCompany, createdAccount.company) },
                    { assertEquals(newCountry, createdAccount.country) },
                    { assertEquals(newTezosAccount.publicKey, createdAccount.publicKey) },
                    { assertEquals(newTezosAccount.privateKey, createdAccount.privateKey) },
                    { assertEquals(bobPublicAddress, createdAccount.hash) }
                )
                assertEquals(
                    accountsInitialCount + 1,
                    mongoTemplate.getCollection("accounts").awaitSingle().countDocuments().awaitSingle()
                )
            }
        }

        @Test
        fun createAccountWithoutTezosKeysTest() {
            val toCreate = AccountCreate(
                login = newLogin, email = newEmail, password = newPassword, fullName = newFullName,
                company = newCompany, country = newCountry, tezosAccount = null
            )

            runBlocking {
                val createdAccount = accountService.createAccount(adminAccount, toCreate)

                assertAll("createdAccount",
                    { assertEquals(newLogin, createdAccount.login) },
                    { assertEquals(newEmail, createdAccount.email) },
                    { assertEquals(newPassword, createdAccount.password) },
                    { assertEquals(newFullName, createdAccount.fullName) },
                    { assertEquals(newCompany, createdAccount.company) },
                    { assertEquals(newCountry, createdAccount.country) },
                    { assertNull(createdAccount.publicKey) },
                    { assertNull(createdAccount.privateKey) },
                    { assertNull(createdAccount.hash) }
                )
                assertEquals(
                    accountsInitialCount + 1,
                    mongoTemplate.getCollection("accounts").awaitSingle().countDocuments().awaitSingle()
                )
            }
        }

        @Test
        fun `cannot create an account with disposable email`() {
            val spammer = AccountCreate(
                login = newLogin, email = "spam@trashmail.com", password = newPassword, fullName = newFullName,
                company = newCompany, country = newCountry, tezosAccount = null
            )
            val spammer2 = spammer.copy(email = "jonny@yopmail.com")

            assertThrows<EmailAddressIsDisposableException> {
                runBlocking { accountService.createAccount(adminAccount, spammer) }
            }

            assertThrows<EmailAddressIsDisposableException> {
                runBlocking { accountService.createAccount(adminAccount, spammer2) }
            }
        }

        @Test
        fun createAccountWithExistingLoginTest() {
            val login = accountLogin1
            val toCreate = AccountCreate(
                login = login, email = newEmail, password = newPassword, fullName = newFullName,
                company = newCompany, country = newCountry, tezosAccount = newTezosAccount
            )

            assertThrows<DuplicateKeyException> { runBlocking { accountService.createAccount(adminAccount, toCreate) } }
        }

        @Test
        fun createAccountWithExistingEmailTest() {
            val email = accountEmail1
            val toCreate = AccountCreate(
                login = newLogin, email = email, password = newPassword, fullName = newFullName,
                company = newCompany, country = newCountry, tezosAccount = newTezosAccount
            )

            assertThrows<DuplicateKeyException> { runBlocking { accountService.createAccount(adminAccount, toCreate) } }
        }

        @Test
        fun `create account with incorrect tezos keys`() {
            val incorrectTezosAccount = TezosAccount(
                publicKey = incorrectPublicKey,
                privateKey = incorrectPrivateKey
            )
            val email = accountEmail1
            val toCreate = AccountCreate(
                login = newLogin, email = email, password = newPassword, fullName = newFullName,
                company = newCompany, country = newCountry, tezosAccount = incorrectTezosAccount
            )

            assertThrows<IncorrectTezosAccountException> {
                runBlocking {
                    accountService.createAccount(
                        adminAccount,
                        toCreate
                    )
                }
            }
        }

        @Test
        fun `create account with invalid tezos keys`() {
            val invalidTezosAccount = TezosAccount(
                publicKey = bobPublicKey,
                privateKey = alicePrivateKey
            )
            val email = accountEmail1
            val toCreate = AccountCreate(
                login = newLogin, email = email, password = newPassword, fullName = newFullName,
                company = newCompany, country = newCountry, tezosAccount = invalidTezosAccount
            )

            assertThrows<IncorrectTezosAccountException> {
                runBlocking {
                    accountService.createAccount(
                        adminAccount,
                        toCreate
                    )
                }
            }
        }

        @Test
        fun `create account with non existent tezos keys`() {
            val nonExistentTezosAccount = TezosAccount(
                publicKey = nonExistentPublicKey,
                privateKey = nonExistentPrivateKey
            )
            val email = accountEmail1
            val toCreate = AccountCreate(
                login = newLogin, email = email, password = newPassword, fullName = newFullName,
                company = newCompany, country = newCountry, tezosAccount = nonExistentTezosAccount
            )

            assertThrows<IncorrectTezosAccountException> {
                runBlocking {
                    accountService.createAccount(
                        adminAccount,
                        toCreate
                    )
                }
            }
        }

    }

    @Nested
    inner class GetAccounts {

        @Test
        fun `Admin can list all accounts`() {
            val accounts = runBlocking { accountService.getAccounts(adminAccount).count() }

            assertEquals(accountsInitialCount, accounts.toLong())
        }

        @Test
        fun `Non admin cannot list all accounts`() {
            assertThrows<MissingRightException> { runBlocking { accountService.getAccounts(nonAdminAccount) } }
        }

        @Test
        fun `Non admin cannot get another account`() {
            assertThrows<MissingRightException> {
                runBlocking {
                    accountService.getAccount(
                        nonAdminAccount,
                        accountId1
                    )
                }
            }

            assertThrows<MissingRightException> {
                runBlocking {
                    accountService.getAccountByLoginOrEmail(
                        nonAdminAccount,
                        accountEmail1
                    )
                }
            }

            assertThrows<MissingRightException> {
                runBlocking {
                    accountService.getAccountByLoginOrEmail(
                        nonAdminAccount,
                        accountLogin1
                    )
                }
            }
        }

        @Test
        fun `Admin can get another account`() {
            val account = runBlocking { accountService.getAccount(adminAccount, accountId1) }
            checkAccount(account1, account)
        }

        @Test
        fun `Non admin can list self`() {
            val account = runBlocking { accountService.getAccount(account1, accountId1) }
            checkAccount(account1, account)

            val account2 = runBlocking { accountService.getAccountByLoginOrEmail(account1, accountEmail1) }
            checkAccount(account1, account2)

            val account3 = runBlocking { accountService.getAccountByLoginOrEmail(account1, accountLogin1) }
            checkAccount(account1, account3)
        }

        private fun checkAccount(expected: Account, account: Account?) {
            assertNotNull(account)
            account as Account
            assertAll("account",
                { assertEquals(expected.login, account.login) },
                { assertEquals(expected.email, account.email) },
                { assertEquals(expected.password, account.password) },
                { assertEquals(expected.fullName, account.fullName) })
        }

    }

    @Nested
    inner class SetTezosAccountTest {
        @Test
        fun `cannot set tezos account if it's already defined`() {
            val tezosAccount = TezosAccount(publicKey = "pub", privateKey = "priv")

            assertThrows<TezosAccountAlreadyDefinedException> {
                runBlocking { accountService.setTezosAccount(accountId1, tezosAccount) }
            }
            assertThrows<TezosAccountAlreadyDefinedException> {
                runBlocking { accountService.setTezosAccount(accountId3, tezosAccount) }
            }
            assertThrows<TezosAccountAlreadyDefinedException> {
                runBlocking { accountService.setTezosAccount(accountId5, tezosAccount) }
            }
            assertThrows<TezosAccountAlreadyDefinedException> {
                runBlocking { accountService.setTezosAccount(accountId6, tezosAccount) }
            }

            // Verifying rollback
            val account = runBlocking { accountService.getAccount(adminAccount, accountId1) }
            assertNotNull(account)
            account as Account
            assertAll(
                "account",
                { assertEquals(accountPublicKey1, account.publicKey) },
                { assertEquals(accountPrivateKey1, account.privateKey) },
                { assertEquals(accountHash1, account.hash) }
            )
        }

        @Test
        fun `set tezos account`() {
            val tezosAccount = TezosAccount(publicKey = bobPublicKey, privateKey = bobPrivateKey)
            val patchedAccount = runBlocking { accountService.setTezosAccount(accountId4, tezosAccount) }

            assertAll(
                "account",
                { assertEquals(tezosAccount.publicKey, patchedAccount.publicKey) },
                { assertEquals(tezosAccount.privateKey, patchedAccount.privateKey) }
            )
        }

        @Test
        fun `set tezos account with incorrect tezos keys`() {
            val tezosAccount = TezosAccount(publicKey = incorrectPublicKey, privateKey = incorrectPrivateKey)
            assertThrows<IncorrectTezosAccountException> {
                runBlocking { accountService.setTezosAccount(accountId7, tezosAccount) }
            }
        }

        @Test
        fun `set tezos account with invalid tezos keys`() {
            val tezosAccount = TezosAccount(publicKey = bobPublicKey, privateKey = alicePrivateKey)
            assertThrows<IncorrectTezosAccountException> {
                runBlocking { accountService.setTezosAccount(accountId7, tezosAccount) }
            }
        }

        @Test
        fun `set tezos account with non existent tezos keys`() {
            val tezosAccount = TezosAccount(publicKey = nonExistentPublicKey, privateKey = nonExistentPrivateKey)
            assertThrows<IncorrectTezosAccountException> {
                runBlocking { accountService.setTezosAccount(accountId7, tezosAccount) }
            }
        }

    }

    @Nested
    inner class PatchAccountTest {

        private val newLogin = "newLogin"
        private val newEmail = "newEmail"
        private val newPassword = "newPassword"
        private val newFullName = "newFullName"
        private val newCompany = "newCompany"
        private val newCountry = "newCountry"
        private val newIsAdmin = true
        private val newDisabled = true
        private val newSignatureLimit = 5

        @Test
        fun patchAccountTest() {
            val toPatch = AccountPatch(password = newPassword)

            val patchedAccount = runBlocking { accountService.patchAccount(adminAccount, accountId1, toPatch) }

            assertAll("patchedAccount",
                { assertEquals(accountLogin1, patchedAccount.login) },
                { assertEquals(accountEmail1, patchedAccount.email) },
                { assertEquals(newPassword, patchedAccount.password) },
                { assertEquals(accountFullName1, patchedAccount.fullName) })
        }

        @Test
        fun `cannot patch an account with disposable email`() {
            val spammer = AccountPatch(email = "spam@trashmail.com")
            val spammer2 = AccountPatch(email = "jonny@yopmail.com")

            assertThrows<EmailAddressIsDisposableException> {
                runBlocking { accountService.patchAccount(adminAccount, accountId1, spammer) }
            }

            assertThrows<EmailAddressIsDisposableException> {
                runBlocking { accountService.patchAccount(adminAccount, accountId1, spammer2) }
            }
        }

        @Test
        fun patchAccountFullTest() {
            val toPatch = AccountPatch(
                login = newLogin, email = newEmail, password = newPassword, fullName = newFullName,
                company = newCompany, country = newCountry,
                isAdmin = newIsAdmin, disabled = newDisabled
            )

            val patchedAccount = runBlocking { accountService.patchAccount(adminAccount, accountId1, toPatch) }

            assertAll("patchedAccount",
                { assertEquals(newLogin, patchedAccount.login) },
                { assertEquals(newEmail, patchedAccount.email) },
                { assertEquals(newPassword, patchedAccount.password) },
                { assertEquals(newFullName, patchedAccount.fullName) },
                { assertEquals(newCompany, patchedAccount.company) },
                { assertEquals(newCountry, patchedAccount.country) },
                { assertEquals(newIsAdmin, patchedAccount.isAdmin) },
                { assertEquals(newDisabled, patchedAccount.disabled) }

            )
        }

        @Test
        fun patchAccountWithSignatureLimit() {
            val toPatch = AccountPatch(
                login = newLogin, email = newEmail, password = newPassword, fullName = newFullName,
                company = newCompany, country = newCountry,
                isAdmin = newIsAdmin, disabled = newDisabled, signatureLimit = newSignatureLimit
            )

            val patchedAccount = runBlocking { accountService.patchAccount(adminAccount, accountId1, toPatch) }

            assertAll("patchedAccount",
                { assertEquals(newLogin, patchedAccount.login) },
                { assertEquals(newEmail, patchedAccount.email) },
                { assertEquals(newPassword, patchedAccount.password) },
                { assertEquals(newFullName, patchedAccount.fullName) },
                { assertEquals(newCompany, patchedAccount.company) },
                { assertEquals(newCountry, patchedAccount.country) },
                { assertEquals(newIsAdmin, patchedAccount.isAdmin) },
                { assertEquals(newDisabled, patchedAccount.disabled) },
                { assertEquals(newSignatureLimit, patchedAccount.signatureLimit) }
            )
        }

        @Test
        fun patchNonexistentAccountTest() {
            val toPatch = AccountPatch(password = newPassword)

            assertThrows<EntityNotFoundException> {
                runBlocking {
                    accountService.patchAccount(
                        adminAccount,
                        accountNonexistentId,
                        toPatch
                    )
                }
            }
        }
    }

    @Nested
    inner class DeleteAccountTest {

        @Test
        fun deleteAccountTest() {
            runBlocking {
                accountService.deleteAccount(adminAccount, accountId1)

                assertEquals(
                    accountsInitialCount - 1,
                    mongoTemplate.getCollection("accounts").awaitSingle().countDocuments().awaitSingle()
                )
            }
        }

        @Test
        fun deleteNonexistentAccountTest() {
            assertThrows<EntityNotFoundException> {
                runBlocking {
                    accountService.deleteAccount(
                        adminAccount,
                        accountNonexistentId
                    )
                }
            }
        }
    }

    @ParameterizedTest
    @MethodSource
    fun checkEmailIsNotDisposable(email: String) {
        accountService as AccountServiceImpl
        assertThrows<EmailAddressIsDisposableException> {
            accountService.checkEmail(email)
        }
    }

    fun checkEmailIsNotDisposable(): Stream<Arguments> {
        return Stream.of(
            Arguments.of("john@trashmail.com"),
            Arguments.of("test@yopmail.com"),
            Arguments.of("julien@frapmail.com"),
            Arguments.of("sam@jetable.com"),
            Arguments.of("paul@yopmail.fr")
        )
    }
}
