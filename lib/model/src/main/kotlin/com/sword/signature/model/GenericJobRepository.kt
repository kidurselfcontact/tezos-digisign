package com.sword.signature.model

import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.data.querydsl.ReactiveQuerydslPredicateExecutor

interface GenericJobRepository<T> : ReactiveMongoRepository<T, String>,
    ReactiveQuerydslPredicateExecutor<T>
