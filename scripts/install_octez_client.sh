#!/usr/bin/env bash

output_dir=$(dirname "$0")/bin
octez_client=$output_dir/octez-client
octez_client_url=https://github.com/serokell/tezos-packaging/releases/latest/download/octez-client

check_octez_client () {
  if [ -f "$octez_client" ]; then
    read -rp "Octez-client is already installed. Do you want to update? [y/n]" prompt
    if [[ "$prompt" =~  [yY](es)* ]]; then
      install_octez_client
    fi
  else
    install_octez_client
  fi
}

install_octez_client () {
  /usr/bin/wget -O "$octez_client" "$octez_client_url"
  chmod u+x "$octez_client"
  import_sandbox_accounts
}

import_sandbox_accounts() {
  $octez_client import secret key alice unencrypted:edsk3QoqBuvdamxouPhin7swCvkQNgq4jP5KZPbwWNnwdZpSpJiEbq --force
  $octez_client import secret key bob unencrypted:edsk3RFfvaFaxbHx8BMtEW1rKQcPtDML3LXjNqMNLCzC3wLC1bWbAt --force
}

check_octez_client
exit 0
